let express = require('express')
let request = require('request')
let httpProxy = require('http-proxy')
let apiProxy = httpProxy.createProxyServer()
let app = express()
let backend = "http://localhost:9000"

function errorLogger(err, req, res, next) {
  console.error(err.stack)
  next(err)
}

function logger(req, res, next) {
  console.info("datetime", req.ip, "status", req.url)
  next()
}

app.use(errorLogger)
app.use(logger)

app.use(express.static('app'))

app.all('/api/*', (req, res) => {
  apiProxy.web(req, res, {target: backend})
})

let server = app.listen(9999, () => {
  console.log("Server started at http://localhost:9999")
})
