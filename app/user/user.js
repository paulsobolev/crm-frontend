(function (angular) {
    "use strict";

    angular.module('crm.user', [])

        .config(function($urlRouterProvider, $stateProvider) {
            $stateProvider.state('login', {
                url: "/login",
                templateUrl: 'user/login.html',
                controller: LoginController
            })
        })

        .factory('User', ['$resource', '$http', '$state', function($resource, $http, $state) {
            var defaultName = 'Guest';
            var self = this;

            self.name = defaultName;
            self.fullName = defaultName;
            self.auth = false;
            self.roles = [];

            self.ROLES = {
                ADMIN: 'admin',
                ACCOUNTANT: 'admin',
                HR: 'admin',
                SUPERVISOR: 'supervisor',
                MANAGER: 'manager'
            };

            self.objects = $resource('/api/user/:id', null);

            self.login = function(name, pass, successCallback, errorCallback) {
                $http.post('/api/login', {username: name, password: pass}, {withCredentials: true})
                    .success(function (data) {
                        self.auth = true;
                        self.name = data.userName;
                        self.fullName = data.fullName;
                        self.role = data.role;
                        self.roles = data.roles;

                        if (angular.isFunction(successCallback)) {
                            successCallback(data);
                        }
                    })
                    .error(function (data) {
                        if (angular.isFunction(errorCallback)) {
                            errorCallback(data);
                        }
                    });
            };

            self.logout = function () {
                $http.get('/api/logout', {withCredentials: true})
                    .then(function () {
                        self.auth = false;
                        self.name = defaultName;
                        self.fullName = defaultName;
                        self.roles = [];
                        $state.go('login');
                    });
            };

            self.can = function(role) {
                var accessGranted = false;
                var roles = [];

                if (role instanceof Array) {
                    roles = role;
                } else {
                    roles = [role];
                }

                roles.forEach(function(role) {
                    var apiRole = self.ROLES[role];
                    if (apiRole && self.roles.indexOf(apiRole) > -1) {
                        accessGranted = true;
                    }
                });

                return accessGranted;
            };

            self.is = function(role) {
                var apiRole = self.ROLES[role];
                return apiRole && self.role.id.toLowerCase() === apiRole.toLowerCase();
            };

            return self;
        }])

        .run(function($rootScope, User) {
            $rootScope.user = User;
        });

    // Controllers
    function LoginController($scope, $rootScope, $state, User) {
        var showErrors = function () {
            $scope.username = $scope.password = '';
            $scope.errorMsg = 'В доступе отказано'
        };

        var redirect = function () {
            var stateName = $rootScope.redirect && $rootScope.redirect.name ? $rootScope.redirect.name : 'index';
            var stateParams = $rootScope.redirect && $rootScope.redirect.params ? $rootScope.redirect.params : {};
            delete($rootScope.redirect);
            $state.go(stateName, stateParams);
        };

        User.login('', '', redirect);

        $scope.submit = function () {
            $scope.errorMsg = '';
            User.login($scope.username, $scope.password, redirect, showErrors);
        }
    }

})(window.angular);
