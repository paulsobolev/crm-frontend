(function(angular) {
    "use strict";

    angular.module("crm.reports", ['crm.timeboard'])

    .filter('complex', function() {
        return function(items, filters) {
            if (!items || !items.length) {
                return [];
            }

            if (filters) {
                _.each(filters, function(filter) {
                    var key = filter.id;
                    var values = filter.value;
                    if (!(values instanceof Array)) {
                        values = values.id ? [values] : [];
                    }

                    if (filter.active) {
                        items = _.filter(items, function(item) {
                            return !values.length || _.find(values, function(v) {
                                return v && item.$employment[key].id && v.id.toString() == item.$employment[key].id.toString();
                            });
                        });
                    }
                });
            }

            return items;
        };
    })

    .config(function($stateProvider) {
        $stateProvider
            .state("reports", {
                url: "/reports",
                abstract: true,
                template: '<ui-view/>'
            })

            .state('reports.timesheet', {
                url: "/timesheet",
                templateUrl: "reports/timesheet.html",
                controller: TimeSheetReportController
            })

            .state('reports.paysheet', {
                url: "/paysheet",
                templateUrl: "reports/paysheet.html",
                controller: TimeSheetReportController
            })

            .state('reports.payroll', {
                url: "/payroll",
                templateUrl: "reports/payroll.html",
                controller: PayrollController
            })

            .state('reports.salary', {
                url: "/salary",
                templateUrl: "reports/salary.html",
                controller: SalaryController
            });
    });

    function TimeSheetReportController($scope, $rootScope, $filter, Storage, extendModel) {

        $scope.models = [];
        $scope.loading = true;

        var storage = Storage.load(['persons','projects','departments','positions', 'presets','employments'], true, function() {
            $scope.loading = false;
        });

        var types = [{id: 1, name: "Посменный"}, {id: 2, name: "Почасовой"}];

        var filterSettings = {
            multi: {
                enableSearch: true,
                displayProp: "name",
                scrollable: true
            },
            single: {
                displayProp: "name",
                selectionLimit: 1,
                smartButtonMaxItems: 1
            }
        };

        $scope.filters = [
            {id: 'person', name: 'Сотрудник', value: [], active: false, options: filterSettings.multi, source: storage.persons},
            {id: 'project', name: 'Проект', value: [], active: false, options: filterSettings.multi, source: storage.projects},
            {id: 'department', name: 'Отдел', value: [], active: false, options: filterSettings.multi, source: storage.departments},
            {id: 'position', name: 'Должность', value: [], active: false, options: filterSettings.multi, source: storage.positions},
            {id: 'type', name: 'График', value: {}, active: false, options: filterSettings.single, source: types}
        ];

        $scope.periodFormat = "MMMM YYYY";
        $scope.period = moment.utc();
        $scope.$lastDay = $scope.period.daysInMonth();

        $scope.$on('document.click.event', function() {
            $scope.datePickerVisible = false;
            $scope.filterSelectorVisible = false;
        });

        $scope.toggleDatePicker = function(event) {
            event.stopPropagation();
            $scope.datePickerVisible = !$scope.datePickerVisible;
        };

        $scope.showDatePicker = function(event) {
            event.stopPropagation();
            $scope.datePickerVisible = true;
        };

        $scope.toggleFilterSelector = function(event) {
            event.stopPropagation();
            $scope.filterSelectorVisible = !$scope.filterSelectorVisible;
        };

        $scope.$watch("period", function(period) {
            if (!(period instanceof moment)) {
                return;
            }

            var newPeriodDays = [];
            var lastDay = period.daysInMonth();

            for (var date = 1; date <= lastDay; date++) {
                var day = period.date(date);

                newPeriodDays.push({
                    number: date,
                    weekDay: day.format("dd"),
                    weekEnd: day.day() === 0 || day.day() === 6, // sunday = 0, saturday = 6
                });
            }

            $scope.$lastDay = lastDay;

            loadData(function() {
                $scope.days = newPeriodDays;
            });
        });

        $scope.$watchCollection("filteredModels", function(filters) {
            $rootScope.$broadcast('app.timeboard.models.rendered');
        });

        var collectPeriod = function() {
            return {
                year: $scope.period.format("YYYY"),
                month: $scope.period.format("MM")
            };
        };

        var timesheets;

        var loadData = function(callback) {
            timesheets = Storage.$timesheets.query(collectPeriod(), function() {
                if (callback) {
                    callback();
                }

                updateModels();
            });
        };

        var updateModels = function() {
            var currentPeriod = $scope.period.format('YYYY-MM');

            $scope.models = [];
            $scope.presets = $filter('filter')(storage.presets, {
                status: 1
            });

            var employments = $filter('filter')(storage.employments, {
                status: 1
            });

            _.each(employments, function(employment) {
                var timesheet = _.find(timesheets, function(t) {
                    return t.employment_id === employment.id && t.period === currentPeriod;
                });

                if (!timesheet) {
                    timesheet = new Storage.$timesheets();
                    timesheet.period = currentPeriod;
                    timesheet.employment_id = employment.id;
                }

                var model = extendModel(timesheet, $scope.presets, $scope.period);
                model.$employment = employment;

                $scope.models.push(model);

                model.calculate();
            });
        };

        $scope.getTotal = function(items, field) {
            var values = _.map(items, function(item) {
                return item.get(field);
            });

            var sum = _.reduce(values, function(total, value) {
                return value && parseFloat(value) >= 0 ? total + parseFloat(value) : total;
            }, 0);

            return sum.toFixed(2);
        };

        function localeString(x, sep, grp) {
            var sx = ('' + x).split('.'),
                s = '',
                i, j;
            sep || (sep = ' '); // default seperator
            grp || grp === 0 || (grp = 3); // default grouping
            i = sx[0].length;
            while (i > grp) {
                j = i - grp;
                s = sep + sx[0].slice(j, i) + s;
                i = j;
            }
            s = sx[0].slice(0, i) + s;
            sx[0] = s;
            return sx.join(',');
        }

        $scope.toNumber = function(value) {
            return value ? localeString(value) : "";
        };
    }

    function PayrollController($scope, $rootScope, Storage, extendModel, $filter, $http) {

        $scope.loading = true;
        $scope.storage = Storage.load(['projects'], false, function() {
            var active = $filter('filter')($scope.storage.projects, {status:1});
            var ordered = $filter('orderBy')(active, 'name');
            $scope.storage.projects = ordered;
            $scope.loading = false;
        });

        $scope.projectSelector = {
            displayProp: "name",
            selectionLimit: 1,
            smartButtonMaxItems: 1,
            enableSearch: true,
            scrollable: true,
            externalIdProp: '',
            smartButtonTextConverter: function(name, project) {
                return 'Проект: ' + project.name + ' (' + project.type.name.toLowerCase() + ')'
            }
        };
        $scope.project = {};

        $scope.periodFormat = "MMMM YYYY";
        $scope.period = moment.utc();

        $scope.$watch("period", function() {
            loadData();
        });

        $scope.$watch("project.id", function() {
            loadData();
        });

        $scope.rows = [
            {id: 'admin', name: 'Администрация'},
            {id: 'salary', name: 'Оклады'},
            {id: 'food', name: 'Питание'},
            {id: 'sick', name: 'Больничные'},
            {id: 'vacation', name: 'Отпуск'},
            {id: 'reward', name: 'Премии'},
            {id: 'charge', name: 'Удержания'}
        ];

        function loadData() {
            $scope.loading = true;
            if (!($scope.period instanceof moment) || !$scope.project.id) {
                return;
            }

            var projects = [$scope.project.id];
            $scope.project.subProjects.forEach(function(sub) {
                projects.push(sub.id);
            });

            $scope.parentCol = {};
            $scope.cols = [];

            $http({
                method: 'GET',
                url: '/api/projects/payroll',
                params: {
                    'period': $scope.period.format('YYYY-MM'),
                    'projectIds[]': projects
                }
            }).then(function successCallback(response) {
                    parseData(response.data);
                    $scope.loading = false;
                }, function errorCallback(response) {
                    $scope.loading = false;
                    console.log('Error', response);
                });
        }

        function parseData(data) {
            $scope.parentCol = _.find(data, function(d) { return d.id === $scope.project.id});

            $scope.parentCol.periodName = $scope.period.format('MMMM');

            data.forEach(function(datum) {
                if (datum.id !== $scope.project.id) {
                    $scope.cols.push(datum);

                    $scope.rows.forEach(function(row) {
                        $scope.parentCol[row.id] = parseFloat($scope.parentCol[row.id]) + parseFloat(datum[row.id]);
                    });
                }
            });
        }

        $scope.total = function(col) {
            var result = 0.00;
            if (col) {
                $scope.rows.forEach(function(row) {
                    var value = parseFloat(col[row.id]);
                    result = row.id === 'charge' ? result - value : result + value;
                });
            }

            return result;
        }
    }

    function SalaryController($scope, Storage, $http) {
        $scope.loading = true;

        $scope.periodFormat = "MMMM YYYY";
        $scope.period = moment.utc();

        $scope.$watch("period", function() {
            loadData();
        });

        $scope.storage = Storage.load(['departments', 'projects'], true, function() {
            loadData();
        });

        function loadData() {
            if (!$scope.period instanceof moment) {
                return;
            }

            var projects = $scope.storage.projects;
            var activeProjects = projects.filter(function(project) {
                return project.status > 0;
            })
            var projectIds = activeProjects.map(function(project) { return project.id; });

            $http({
                method: 'GET',
                url: '/api/projects/payroll',
                params: {
                    'period': $scope.period.format('YYYY-MM'),
                    'projectIds[]': projectIds
                }
            }).then(function successCallback(response) {
                    parseData(response.data);
                    $scope.loading = false;
                }, function errorCallback(response) {
                    $scope.loading = false;
                    console.log('Error', response);
                });
        }

        function parseData(rawData) {
            var models = [];
            rawData.forEach(function(project) {
                var model = _.find(models, function(m) {
                    return m.name === project.company;
                });

                project.size = project.departments.length > 1 ? project.departments.length + 1 : 1;

                if (!model) {
                    model = {
                        name: project.company,
                        projects: []
                    };
                    models.push(model);
                }

                model.projects.push(project);
                model.size = model.projects.length > 1 ? 1 : 0;
                model.projects.forEach(function(pr) {
                    model.size += pr.size;
                });
            });

            $scope.models = models;
            $scope.projects = rawData;
        }

        $scope.total = function(collection, property) {
            var result = 0.00;
            if (collection && property) {
                collection.forEach(function(model) {
                    var value = parseFloat(model[property] || 0);
                    result = result + value;
                });
            }

            return result;
        }
    }

})(window.angular);
