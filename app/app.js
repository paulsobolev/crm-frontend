(function(angular) {
    'use strict';

    angular.module('crm', [
        'ngResource',
        'ngSanitize',
        'ui.router',
        'ui.bootstrap',
        'ui.select',
        'colorpicker.module',
        'dropdown-multiselect',
        'datePicker',
        'crm.user',
        'crm.company',
        'crm.timeboard',
        'crm.reports',
        'crm.import',
        'crm.ui'
    ])

    .config(function($urlRouterProvider, $stateProvider) {
        $urlRouterProvider.otherwise("/timeboard");

        $stateProvider.state('index', {
            url: ""
        });
    })

    .factory("Storage", ["$resource", "$q", "$log", function($resource, $q, $log) {
        var self = {};

        function createResource(name) {
            var resource = $resource('/api/' + name + '/:id', {id: '@id'}, {'update': {method: 'PUT'}});

            resource.prototype.save = function(params, success) {
                var entry = this;
                var action = entry.id ? '$update' : '$save';

                delete entry.$original;
                delete entry.$edit;

                return entry[action](params || {}, function() {
                    entry.backup();

                    if (success) {
                        success.apply(entry, arguments);
                    }
                });
            };

            resource.prototype.isNew = function() {
                return !this.id;
            };

            resource.prototype.isUnsaved = function() {
                return (this.$original && !angular.equals(this, this.$original)) || this.isNew();
            };

            resource.prototype.isActive = function() {
                return this.status === 1;
            };

            resource.prototype.isDeleted = function() {
                return this.status === 0;
            };

            resource.prototype.backup = function() {
                var original = {};
                for (var key in this) {
                    if (key[0] != '$' && this.hasOwnProperty(key)) {
                        original[key] = this[key];
                    }
                }
                this.$original = original;
            };

            resource.prototype.restore = function() {
                if (this.$original) {
                    for (var key in this.$original) {
                        if (key[0] != '$' && this.hasOwnProperty(key)) {
                            this[key] = this.$original[key];
                        }
                    }
                }
            };

            return resource;
        }

        function updateCollection(name, activeOnly, callback) {
            var instance = this;
            var params = activeOnly ? {activeOnly: true} : {};

            instance[name] = instance[name] || [];

            // customize query to load active/deleted entries
            var request = self["$" + name].query(params, function(collection) {
                instance[name] = collection;

                if (callback) {
                    callback(collection);
                }
            });

            return request.$promise;
        }

        ['employments','persons','positions','projects','departments','presets','timesheets','companies'].forEach(function(resourceName) {
            self['$' + resourceName] = createResource(resourceName);
        });

        self.load = function(names, activeOnly, callback) {
            var instance = {};
            var promises = [];

            names.forEach(function(name) {
                var promise = updateCollection.call(instance, name, activeOnly);
                promises.push(promise);
            });

            $q.all(promises).then(function(collectionsArray) {
                if (callback) {
                    callback(collectionsArray);
                }
            });

            instance.update = updateCollection;

            return instance;
        };

        return self;
    }])

    .factory('DataProvider', ['$resource', '$rootScope', '$timeout', '$filter', function($resource, $rootScope, $timeout, $filter) {

        var collections = [
            'departments',
            'employments',
            'persons',
            'positions',
            'presets',
            'projects'
        ];

        var provider = {};
        var resources = {};

        function createResource(name) {
            var resource = $resource('/api/' + name + '/:id', {
                id: '@id'
            }, {
                'update': {
                    method: 'PUT'
                }
            });

            resource.prototype.save = function(params, success) {
                var action = this.id ? '$update' : '$save';
                var _this = this;

                delete this.$original;
                delete this.$edit;

                return this[action](params || {}, function() {
                    _this.backup();
                    if (success && _.isFunction(success)) {
                        success.apply(_this, arguments);
                    }
                });
            };

            resource.prototype.isNew = function() {
                return !this.id;
            };

            resource.prototype.isUnsaved = function() {
                return (this.$original && !angular.equals(this, this.$original)) || this.isNew();
            };

            resource.prototype.isActive = function() {
                return this.status === 1;
            };

            resource.prototype.isDeleted = function() {
                return this.status === 0;
            };

            resource.prototype.backup = function() {
                var original = {};
                for (var key in this) {
                    if (key[0] != '$' && this.hasOwnProperty(key)) {
                        original[key] = this[key];
                    }
                }
                this.$original = original;
            };

            resource.prototype.restore = function() {
                if (this.$original) {
                    for (var key in this.$original) {
                        if (key[0] != '$' && this.hasOwnProperty(key)) {
                            this[key] = this.$original[key];
                        }
                    }
                }
            };

            return resource;
        }

        function fetchData(name) {
            if (name && resources[name]) {
                fetchCollection(name);
            } else {
                _.each(collections, function(collectionName) {
                    fetchCollection(collectionName);
                });
            }
        }

        function fetchCollection(name) {
            provider[name] = resources[name].query({}, function() {
                _.each(provider[name], function(model) {
                    model.backup();
                });
            });
        }

        _.each(collections, function(name) {
            resources[name] = createResource(name);
            provider['$' + name] = resources[name];
        });

        provider.create = function(name) {
            if (_.indexOf(collections, name) > -1) {
                return new resources[name]();
            }
            return null;
        };

        $rootScope.$on("data.collection.fetch", function(event, name) {
            event.stopPropagation();

            fetchData(name);
        });

        provider.timesheets = createResource("timesheets");

        fetchData();

        return provider;
    }])

    .run(['$document', '$rootScope', '$state', '$stateParams', 'User', function($document, $rootScope, $state, $stateParams, User) {
        moment.locale("ru");

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $document.on("click", function() {
            $rootScope.$apply(function() {
                $rootScope.$broadcast("document.click.event");
            });
        });

        $rootScope.$on('$stateChangeStart', function(e, target, params) {
            if (!User.auth && target.name !== 'login') {
                e.preventDefault();
                $rootScope.redirect = {
                    name: target.name,
                    params: params
                };

                $state.go('login');
            } else if (target.access && !$rootScope.user.can(target.access)) {
                e.preventDefault();
                alert('В доступе отказано');
                $state.go('index');
            }
        });
    }]);

})(window.angular);
