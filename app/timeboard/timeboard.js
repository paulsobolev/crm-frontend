(function(angular) {
    "use strict";

    angular.module("crm.timeboard", [])

    .filter('complex', function() {
        return function(items, filters) {
            if (!items || !items.length) {
                return [];
            }

            if (filters) {
                _.each(filters, function(filter) {
                    var key = filter.id;
                    var values = filter.value;
                    if (!(values instanceof Array)) {
                        values = values.id ? [values] : [];
                    }

                    if (filter.active) {
                        items = _.filter(items, function(item) {
                            return !values.length || _.find(values, function(v) {
                                return v && item.$employment[key].id && v.id.toString() == item.$employment[key].id.toString();
                            });
                        });
                    }
                });
            }

            return items;
        };
    })

    .factory('extendModel', function($timeout) {

        var checkPresets = function(presets, value) {
            return _.find(presets, function(pr) {
                return pr && value && pr.code.toLowerCase() === value.toString().toLowerCase();
            });
        };

        return function(model, presets, period) {
            var extended = {
                $origin: model,
            };

            extended.ngModel = function(name) {
                return function(value) {
                    return arguments.length ? extended.set(name, value) : extended.get(name);
                }
            }

            extended.get = function(field) {
                var fields = field.split(".");
                var value = "";

                if (fields[0]) {
                    var fieldName = fields[0];
                    value = fieldName.startsWith("$") ? this[fieldName] : this.$origin[fieldName];

                    if (fields[1]) {
                        value = value[fields[1]] || "";

                        if (fields[2]) {
                            value = value[fields[2]] || "";
                        }
                    }
                }

                return value;
            };

            extended.set = function(fieldName, value, doNotCalculate, doNotSave) {
                if (fieldName.startsWith("$")) {
                    this[fieldName] = value;
                } else {
                    this.$origin[fieldName] = value;
                }

                if (!doNotCalculate) {
                    this.calculate();
                }

                if (!doNotSave) {
                    this.sync();
                }
            };

            extended.$syncTimer = null;

            extended.$sync = function() {
                var $this = this;
                var addToCollection = $this.$origin.isNew();

                if ($this.$origin.isUnsaved()) {
                    $this.$origin.save({}, function(result) {
                        if (addToCollection) {
                            $this.$origin.id = result.id;
                        }
                    });
                }
            };

            // TODO rewrite this shit
            extended.sync = function() {
                var $this = this;

                $timeout.cancel($this.$syncTimer);
                $this.$syncTimer = $timeout(function() {
                    $this.$sync.apply($this);
                }, 700);
            };

            extended.calculate = function() {
                var $this = this;
                var dayValues = [0.0];
                var daySalaries = [0.0];
                var employment = $this.get('$employment');
                var rates = employment && employment.rates ? employment.rates : [];

                for (var i = 1; i <= period.daysInMonth(); i++) {
                    var dayValue = this.get('day_' + i);
                    var daySalary = 0;

                    if (dayValue) {
                        var inPresets = checkPresets(presets, dayValue);

                        if (!inPresets) {
                            dayValue = parseFloat(dayValue);

                            if (!isNaN(dayValue)) {
                                dayValues.push(dayValue);

                                var dayStart = period.date(i).startOf('day').unix();

                                var rate = _.find(rates, function(r) {
                                    return r.date <= dayStart;
                                }) || rates[0] || null;

                                if (rate && this.get('norm')) {
                                    var n = parseFloat(this.get('norm'));
                                    var r = parseFloat(rate.value);
                                    daySalary = (!isNaN(n) && !isNaN(r)) ? (dayValue / n) * r : 0;
                                }
                            }
                        }
                    }

                    daySalaries[i] = daySalary;
                }

                var totalWorkTime = _.sum(dayValues);
                var totalSalary = $this.get('corrected_total') ? parseFloat($this.get('corrected_total')) : _.sum(daySalaries);
                var totalPayment = 0.0;
                var restPayment = 0.0;

                var payments = 0.0;

                _.each(['fee', 'food', 'sick', 'vacation', 'reward'], function(field) {
                    if ($this.get(field)) {
                        payments += _.toNumber($this.get(field));
                    }
                });

                var charge = $this.get('charge') ? _.toNumber($this.get('charge')) : 0.0;

                this.set('$prepaymentCard', this.get('prepayment_card') || this.get('prepaymentImport'), true, true);

                totalPayment = (totalSalary + payments - charge);

                if (totalPayment) {
                    restPayment = totalPayment;
                    _.each(['prepayment', '$prepaymentCard', 'payment_card'], function(field) {
                        if ($this.get(field)) {
                            restPayment -= _.toNumber($this.get(field));
                        }
                    });
                }

                this.set('$daySalaries', daySalaries, true, true);
                this.set('$totalWorkTime', totalWorkTime || "", true, true);
                this.set('$totalSalary', totalSalary ? totalSalary.toFixed(2) : "", true, true);
                this.set('$totalPayment', totalPayment ? totalPayment.toFixed(2) : "", true, true);
                this.set('$restPayment', restPayment ? restPayment.toFixed(2) : "", true, true);


                this.set('totalSalary', totalSalary ? totalSalary.toFixed(2) : "", true);
                this.set('totalPayment', totalPayment ? totalPayment.toFixed(2) : "", true);
                this.set('restPayment', restPayment ? restPayment.toFixed(2) : "", true);
            };

            return extended;
        };
    })

    .config(function($stateProvider) {
        $stateProvider
            .state("timeboard", {
                url: "/timeboard",
                templateUrl: "timeboard/timeboard.html",
                controller: TimeBoardController
            });
    });

    function TimeBoardController($scope, $rootScope, $filter, Storage, extendModel, User) {
        var timesheets = [];
        var types = [{
            id: 1,
            name: "Посменный"
        }, {
            id: 2,
            name: "Почасовой"
        }];
        var filterSettings = {
            multi: {
                enableSearch: true,
                displayProp: "name",
                scrollable: true
            },
            single: {
                displayProp: "name",
                selectionLimit: 1,
                smartButtonMaxItems: 1
            }
        };
        var models;

        function loadData() {
            var params = {
                year: $scope.period.format("YYYY"),
                month: $scope.period.format("MM")
            };

            $scope.loading = true;

            Storage.$timesheets.query(params, function(result) {
                timesheets = result;
                models = getPreparedModels();
                filterModels();

                $scope.loading = false;
            });
        }

        function filterModels() {
            $scope.models = $filter("complex")(models, $scope.filters);
        }

        function getPreparedModels() {
            var currentPeriod = $scope.period.format('YYYY-MM');
            var models = [];

            $scope.storage.employments.forEach(function(employment) {

                var timesheet = _.find(timesheets, function(t) {
                    return t.employment_id === employment.id && t.period === currentPeriod;
                });

                if (!timesheet) {
                    timesheet = new Storage.$timesheets();
                    timesheet.period = currentPeriod;
                    timesheet.employment_id = employment.id;
                }

                var model = extendModel(timesheet, $scope.storage.presets, $scope.period);
                model.$employment = employment;

                models.push(model);

                model.calculate();

                timesheet.backup();
            });

            return models;
        }

        $scope.ready = false;
        $scope.models = [];

        $scope.loading = true;

        $scope.storage = Storage.load(['employments', 'persons', 'projects', 'departments', 'positions', 'presets'], true, function() {
            $rootScope.$broadcast('app.timeboard.models.rendered');
            $scope.ready = true;
            loadData();
            initFilters();
        });

        $scope.filters = [];

        function initFilters() {
            $scope.filters = [{
                id: 'person',
                name: 'Сотрудник',
                value: [],
                active: false,
                options: filterSettings.multi,
                source: $scope.storage.persons,
                events: {onItemSelect: filterModels, onItemDeselect: filterModels}
            }, {
                id: 'project',
                name: 'Проект',
                value: [],
                active: false,
                options: filterSettings.multi,
                source: $scope.storage.projects,
                events: {onItemSelect: filterModels, onItemDeselect: filterModels}
            }, {
                id: 'department',
                name: 'Отдел',
                value: [],
                active: false,
                options: filterSettings.multi,
                source: $scope.storage.departments,
                events: {onItemSelect: filterModels, onItemDeselect: filterModels}
            }, {
                id: 'position',
                name: 'Должность',
                value: [],
                active: false,
                options: filterSettings.multi,
                source: $scope.storage.positions,
                events: {onItemSelect: filterModels, onItemDeselect: filterModels}
            }, {
                id: 'type',
                name: 'График',
                value: {},
                active: false,
                options: filterSettings.single,
                source: types,
                events: {onItemSelect: filterModels, onItemDeselect: filterModels}
            }];
        }

        $scope.resetFilter = function(filter) {
            filter.active = false
            filter.value = filter.value instanceof Array ? [] : {}
            filterModels()
        }

        $scope.periodFormat = "MMMM YYYY";
        $scope.period = moment.utc();
        $scope.$lastDay = $scope.period.daysInMonth();

        $scope.$on("table.cell.focus.move", function(event, data) {
            var targetRow = null,
                targetCell = null;

            switch (data.direction) {
                case 'left':
                case 'right':
                    targetRow = data.row;
                    targetCell = (data.direction === 'left') ? data.cell - 1 : data.cell + 1;
                    break;
                case 'up':
                case 'down':
                    targetCell = data.cell;
                    targetRow = (data.direction === 'up') ? data.row - 1 : data.row + 1;
                    break;
            }

            if (targetRow !== null && targetCell !== null) {
                $rootScope.$broadcast("table.cell.focus.trigger", {
                    row: targetRow,
                    cell: targetCell
                });
            }
        });

        $scope.$watch("period", function(period) {
            if (!(period instanceof moment)) {
                return;
            }

            var newPeriodDays = [];
            var lastDay = period.daysInMonth();

            for (var date = 1; date <= lastDay; date++) {
                var day = period.date(date);

                newPeriodDays.push({
                    number: date,
                    weekDay: day.format("dd"),
                    weekEnd: day.day() === 0 || day.day() === 6, // sunday = 0, saturday = 6
                    editable: userCanEdit(date)
                });
            }

            $scope.$lastDay = lastDay;
            $scope.days = newPeriodDays;
            $scope.models = [];

            if ($scope.ready) {
                loadData();
            }
        });

        function userCanEdit(dayNumber) {
            var date = $scope.period.date(dayNumber),
                accessGranted = !(User.is('SUPERVISOR') || User.is('MANAGER')),
                minDate,
                maxDate,
                isCurrentMonth = $scope.period.month() === moment().month();

            if (User.is('MANAGER')) {
                minDate = moment().add(-2, 'days').startOf('day');
                maxDate = moment().endOf('day');
                accessGranted = date.isBetween(minDate, maxDate);
            }

            if (User.is('SUPERVISOR')) {
                if (moment().date() < 3) {
                    minDate = moment().add(-1, 'month').set('date', 16).startOf('day');
                    maxDate = isCurrentMonth ? moment().endOf('day') : moment().add(-1, 'month').endOf('month');
                } else if (moment().date() < 17) {
                    minDate = moment().set('date', 1).startOf('day');
                    maxDate = moment().set('date', 15).endOf('day');
                } else {
                    minDate = moment().set('date', 16).startOf('day');
                    maxDate = moment().endOf('month');
                }

                accessGranted = date.isBetween(minDate, maxDate);
            }

            return accessGranted;
        }
    }

})(window.angular);
