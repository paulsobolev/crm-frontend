(function (angular) {
    "use strict";

    angular.module('crm.import', [])

        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.when('/import', '/import/process');

            $stateProvider
                .state('import', {
                    url: "/import",
                    abstract: true,
                    templateUrl: "import/index.html"
                })

                .state('import.settings', {
                    url: "/settings",
                    templateUrl: "import/settings.html",
                    controller: SettingsController,
                    access: ['ADMIN']
                })

                .state('import.process', {
                    url: "/process",
                    templateUrl: "import/process.html",
                    controller: ProcessController,
                    access: ['ADMIN']
                });
        });

    // Controllers
    function SettingsController($rootScope, $scope, $state, Storage, $log) {
        $scope.loading = true;
        $scope.storage = Storage.load(['projects', 'companies'], true, init);

        function init(collections) {
            $scope.loading = false;
            $scope.storage.companies.forEach(function(model) {
                model.backup();
            });
        }

        $scope.add = function () {
            var model = new Storage.$companies();
            model.$edit = true;
            $scope.storage.companies.splice(0,0,model);
        };

        $scope.delete = function (model) {
            if (model.isNew()) {
                $scope.storage.companies = _.without($scope.storage.companies, model);
            }
            else if (confirm("Пожалуйста, подтвердите удаление записи")) {
                model.$delete({}, function() {
                    $scope.storage.companies = _.without($scope.storage.companies, model);
                });
            }
        };

        $scope.getProjectById = function(id) {
            return _.find($scope.storage.projects, function(pr) { return pr.id === id; });
        };
    }

    function ProcessController($scope, $http, $filter, $timeout) {
        $scope.importErrors = [];

        function init() {
            $scope.loading = true;
            $scope.processing = false;
            $scope.enabled = [];

            $http.get('/api/import/scan').then(
                function(response) {
                    $scope.loading = false;
                    $scope.models = prepareModels(response.data);
                }
            );
        }

        function prepareModels(models) {
            return models.map(function(model) {
                model.period = moment(model.periodTs * 1000);
                return model;
            });
        }

        $scope.toggleImport = function(model) {
            model.import = !model.import;
            $scope.enabled = $filter('filter')($scope.models, {import:true});
        }

        $scope.process = function(action) {
            var records = $scope.enabled.map(function(model) {
                return model.id
            });

            action = action || 'data';

            $scope.processing = true;
            $http.post('/api/import/' + action, {records: records}).then(
                function() {
                    init();
                },
                function(response) {
                    if (response.data && response.data.results) {
                        $scope.importErrors = response.data.results;
                    } else {
                        $scope.importErrors = [{
                            message: response.data && response.data.message ?
                                response.data.message :
                                "Запрос не выполнен. Обратитесь к администратору системы"
                        }];
                    }
                    $scope.processing = false;
                }
            );
        }

        init();
    }

})(window.angular);
