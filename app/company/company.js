(function (angular) {
    "use strict";

    angular.module('crm.company', [])

        .filter('belongs', function () {
            return function (items, ids) {
                if (!ids || !items || !ids.length || !items.length) {
                    return [];
                }

                return _.filter(items, function(item) {
                    return _.find(ids, function(id) {
                        return id.toString() == item.id.toString();
                    });
                });
            };
        })

        .filter('findBy', function () {
            return function (items, field, value) {
                return _.filter(items, function(item) {
                    if (!item[field]) {
                        return false;
                    }
                    if (!value) {
                        return true;
                    }
                    return _.find(item[field], function(f) {
                        return f.toString() == value.toString();
                    })
                });
            };
        })

        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.when('/company', '/company/employments/list');

            $stateProvider
                .state('company', {
                    url: "/company",
                    abstract: true,
                    templateUrl: "company/company.html"
                })

                .state('company.employments', {
                    url: "/employments",
                    abstract: true,
                    template: "<ui-view/>"
                })

                .state('company.employments.list', {
                    url: "/list",
                    templateUrl: "company/employments/list.html",
                    data: {
                        collection: 'employments',
                        preload: ['employments', 'persons', 'projects', 'departments', 'positions']
                    },
                    controller: ListController,
                    access: ['ADMIN', 'HR']
                })

                .state("company.employments.create", {
                    url: "/create",
                    templateUrl: "company/employments/create.html",
                    controller: EmploymentController,
                    access: ['ADMIN', 'HR']
                })

                .state("company.employments.update", {
                    url: "/update/:id",
                    templateUrl: "company/employments/create.html",
                    controller: EmploymentController,
                    access: ['ADMIN', 'HR']
                })

                .state('company.persons', {
                    url: "/persons",
                    templateUrl: "company/persons/list.html",
                    data: {
                        collection: 'persons',
                        preload: ['persons']
                    },
                    controller: ListController,
                    access: ['ADMIN', 'HR']
                })

                .state('company.projects', {
                    url: "/projects",
                    templateUrl: "company/projects/list.html",
                    data: {
                        collection: 'projects',
                        preload: ['projects', 'departments']
                    },
                    controller: ListController,
                    access: ['ADMIN']
                })

                .state('company.departments', {
                    url: "/departments",
                    templateUrl: "company/departments/list.html",
                    data: {
                        collection: 'departments',
                        preload: ['projects', 'departments']
                    },
                    controller: ListController,
                    access: ['ADMIN']
                })

                .state('company.positions', {
                    url: "/positions",
                    templateUrl: "company/positions/list.html",
                    data: {
                        collection: 'positions',
                        preload: ['positions']
                    },
                    controller: ListController,
                    access: ['ADMIN']
                })

                .state('company.presets', {
                    url: "/presets",
                    templateUrl: "company/presets/list.html",
                    data: {
                        collection: "presets",
                        preload: ['presets']
                    },
                    controller: ListController,
                    access: ['ADMIN']
                })

                .state("company.employments.terminate", {
                    url: "/:id/terminate",
                    templateUrl: "company/employments/terminate.html",
                    controller: EmploymentController,
                    access: ['ADMIN', 'HR']
                });
        });

    function EmploymentController($scope, $state, $stateParams, Storage, $http) {

        $scope.startDateChanged = function(event, momentObj) {
            $scope.model.startDate = momentObj.unix();
        };

        $scope.endDateChanged = function(event, momentObj) {
            $scope.model.endDate = momentObj.unix();
        };

        if ($stateParams.id) {
            $scope.create = false;
            $scope.model = Storage.$employments.get({id: $stateParams.id}, function() {
                ["person", "project", "department", "position"].forEach(function(name) {
                    $scope.model[name + '_id'] = $scope.model[name].id;
                });

                $scope.model.type = $scope.model.type.id.toString();
                $scope.model.rate = $scope.model.rates[0] ? $scope.model.rates[0].value : '';

                $scope.startDate = moment($scope.model.startDate * 1000);
            });
        } else {
            $scope.create = true;
            $scope.model = new Storage.$employments();
        }

        $scope.storage = Storage.load(['persons', 'projects', 'departments', 'positions'], true);

        $scope.dismiss = function() {
            $state.go('^.list');
        };

        $scope.startDateChanged({}, moment());

        $scope.save = function() {
            $scope.model.save().then(function() {
                $state.go('^.list');
            });
        };

        $scope.terminate = function() {
            var url = '/api/employments/'+$scope.model.id+'/terminate';
            $http.post(url, {endDate: $scope.model.endDate}).then(function() {
                $state.go('^.list');
            });
        };

        $scope.timestampAsDate = function(ts) {
            return moment.unix(ts).format("DD.MM.YYYY");
        };
    }

    // Controllers
    function ListController($rootScope, $scope, $state, $filter, Storage, DataProvider, $log) {
        var currentCollection = $state.current.data.collection;

        function init() {
            $scope.storage = Storage.load($state.current.data.preload);
        }

        $rootScope.$on("storage.updated", init);

        init();

        $scope.add = function () {
            var model = DataProvider.create(currentCollection);
            model.$edit = true;
            $scope.storage[currentCollection].splice(0,0,model);
        };

        $scope.delete = function (model) {
            if (model.isNew()) {
                $scope.storage[currentCollection] = _.without($scope.storage[currentCollection], model);
            }
            else if (confirm("Пожалуйста, подтвердите удаление записи")) {
                model.status = 0;
                model.$delete({}, function() {
                    model.status = 0;
                    model.backup();
                });
            }
        };

        $scope.timestampAsDate = function(ts) {
            return moment.unix(ts).format("DD.MM.YYYY");
        };

        var rateHistoryModelId = null;

        $scope.$on('document.click.event', function() {
            rateHistoryModelId = null;
        });

        $scope.showRateHistory = function(event, model) {
            event.stopPropagation();

            rateHistoryModelId = model.id;
        };

        $scope.hasRateHistory = function(model) {
            return rateHistoryModelId === model.id;
        };

        $scope.restore = function(model) {
            if (confirm("Восстановить запись?")) {
                model.status = 1;
                model.save();
            }
        };
    }

})(window.angular);
