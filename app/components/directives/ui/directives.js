(function(angular) {
    'use strict';

    angular.module('crm.ui', [])
        .value('KEYS', {
            arrows: {
                37: 'left',
                38: 'up',
                39: 'right',
                40: 'down'
            }
        })

        .directive('presetsSelector', ['$document', '$rootScope', '$log',
            function($document, $rootScope, $log) {
                $document.on('click', '.presets-selector', function(event) {
                    event.stopPropagation();
                });

                return {
                    restrict: 'AE',
                    replace: true,
                    scope: {
                        presets: '='
                    },
                    templateUrl: 'components/directives/ui/presets-selector.html',
                    link: function($scope, $element) {
                        var $tableBody = $element.closest('.timeboard-content');

                        function open(position) {
                            $element.removeClass('hidden');
                            $element.css({
                                'top': position.top + 34,
                                'left': position.left,
                                'margin-left': $tableBody.prop('scrollLeft'),
                                'margin-top': $tableBody.prop('scrollTop')
                            });
                        }

                        function close() {
                            $element.addClass('hidden');
                        }

                        $scope.getPresetStyle = function(preset) {
                            return {
                                'color': preset.color,
                                'background-color': preset.bg_color
                            };
                        };

                        $scope.select = function(preset) {
                            $rootScope.$broadcast("table.cell.preset.selected", {
                                preset: preset,
                                $id: $scope.modelScopeId
                            });
                            close();
                        };

                        $scope.$on("table.cell.focus.event", function(event, data) {
                            if (data.showPresets) {
                                $scope.modelScopeId = data.$id;
                                open(data.position);
                            } else {
                                $scope.modelScopeId = null;
                                close();
                            }
                        });

                        $scope.$on("document.click.event", function() {
                            close();
                        });
                    }
                };
            }
        ])

        .directive('endRepeat', ['$rootScope', '$timeout',
            function($rootScope, $timeout) {
                return function($scope, $element, $attrs) {
                    if ($scope.$last) {
                        $timeout(function() {
                            $rootScope.$broadcast('app.timeboard.models.rendered');
                        }, 0);
                    }
                };
            }
        ])

        .directive('timeboard', ['$document', '$rootScope', '$window', '$filter', '$log', 'KEYS',
            function($document, $rootScope, $window, $filter, $log, KEYS) {
                return {
                    restrict: 'AE',
                    link: function($scope, $element, $attrs) {
                        var marginBottom = 10;
                        var $header = $element.find('.timeboard-header');
                        var $content = $element.find('.timeboard-content');
                        var $sidebar = $element.find('.timeboard-sidebar > .content');

                        $scope.activeCell;

                        $scope.$on("table.cell.preset.selected", function(event, data) {
                            if (data.$id === $scope.$id && data.preset) {
                                $scope.activeCell.find('input')
                                    .val(data.preset.code)
                                    .change()
                                    .focus();
                            }
                        });

                        $scope.getPresetStyle = function(code) {
                            var preset = _.find($scope.storage.presets, function(pr) {
                                return pr.code.toLowerCase() === code.toLowerCase();
                            });

                            return preset ? {
                                color: preset.color,
                                backgroundColor: preset.bg_color,
                                fontWeight: "bold"
                            } : {};
                        };

                        $scope.worktime = function(value) {
                            if (value === '' || !value)
                                return '';

                            var clean = value.toString().replace(',', '.').replace(' ', '');
                            var fractionSize = clean.indexOf('.') > -1 ? 1 : 0;

                            return isNaN(clean) ? clean : $filter('number')(clean, fractionSize);
                        }

                        $scope.decimal = function(value) {
                            if (value === '' || !value)
                                return '';

                            var clean = value.toString().replace(',', '.').replace(' ', '');

                            return isNaN(clean) ? clean : $filter('number')(clean, 2);
                        }

                        function scroll() {
                            var scrollTop  = $content.prop('scrollTop');
                            var scrollLeft = $content.prop('scrollLeft');

                            $header.css({
                                transform: 'translateX(' + -scrollLeft + 'px)'
                            })
                            $sidebar.css({
                                transform: 'translateY(' + -scrollTop + 'px)'
                            })
                        }

                        function updateLayout() {
                            $element.css({height: $window.innerHeight - $element.offset().top - marginBottom});

                            var leftMargin = $sidebar.width();

                            $header.css({left: leftMargin + 'px'});
                            $content.css({left: leftMargin + 'px'});
                        }

                        function focusOnCell($cell) {
                            if (!$cell) {
                                return;
                            }

                            if ($scope.activeCell) {
                                $scope.activeCell.removeClass('focus')
                                $scope.activeCell.find('input').blur()
                            }

                            $cell.addClass('focus')
                            $scope.activeCell = $cell
                            $cell.find('input').focus()

                            $rootScope.$broadcast("table.cell.focus.event", {
                                $id: $scope.$id,
                                position: $cell.position(),
                                showPresets: !!$cell.hasClass('worktime')
                            });
                            $scope.$apply();
                        }

                        function scrollContent($cell, direction, fromFixedCell) {
                            var scrollTop = $content.prop('scrollTop')
                            var scrollLeft = $content.prop('scrollLeft')
                            var cellStyle = getComputedStyle($cell[0])
                            var width = parseInt(cellStyle.width, 10)
                            var height = parseInt(cellStyle.height, 10)

                            switch(direction) {
                                case 'up':
                                    scrollTop -= height
                                    break;
                                case 'right':
                                    if (!fromFixedCell) {
                                        scrollLeft += width
                                    }
                                    break;
                                case 'down':
                                    scrollTop += height
                                    break;
                                case 'left':
                                    if (!fromFixedCell) {
                                        scrollLeft -= width
                                    }
                                    break;
                            }

                            $content.prop({
                                scrollLeft: scrollLeft,
                                scrollTop: scrollTop
                            })
                        }

                        function changeActiveCell(direction) {
                            var cell  = $scope.activeCell[0];
                            var row   = cell.parentNode;
                            var table = row.parentNode;
                            var cellIndex = Array.prototype.indexOf.call(row.children, cell);
                            var rowIndex = Array.prototype.indexOf.call(table.children, row);
                            var target;

                            switch(direction) {
                                case 'up':
                                    if (rowIndex > 0) {
                                        target = table.children[rowIndex-1].children[cellIndex];
                                    }
                                    break;
                                case 'right':
                                    if (cellIndex + 1 < row.children.length) {
                                        target = table.children[rowIndex].children[cellIndex + 1];
                                    }
                                    break;
                                case 'down':
                                    if (rowIndex + 1 < table.children.length) {
                                        target = table.children[rowIndex+1].children[cellIndex];
                                    }
                                    break;
                                case 'left':
                                    if (cellIndex > 0) {
                                        target = table.children[rowIndex].children[cellIndex - 1];
                                    }
                                    break;
                            }

                            if (target) {
                                var $target = angular.element(target)
                                scrollContent($scope.activeCell, direction)
                                focusOnCell($target)
                            }
                        }

                        function init() {
                            $element.addClass('timeboard');
                            $content.on('scroll', scroll);

                            $content
                                .on('click', 'td', function(e) {
                                    e.stopPropagation();
                                    focusOnCell(angular.element(this));
                                })
                                .on('focus', 'input', function() {
                                    var $input = angular.element(this);
                                    if ($input.val()) {
                                        $input.select();
                                    }
                                })
                                .on('keydown', 'input', function (e) {
                                    var $input = angular.element(this);
                                    var key = e.which ? e.which : event.keyCode;
                                    if (key == 110 || key == 188) {
                                        e.preventDefault();
                                        var value = $input.val();
                                        $input.val(value + ".");
                                    }
                                });

                            updateLayout();

                            angular.element($window)
                                .on('resize load', function() {
                                    updateLayout();
                                    $scope.$digest();
                                })
                                .on('click', function(e) {
                                    if ($scope.activeCell) {
                                        $scope.activeCell.removeClass('focus');
                                        $scope.activeCell = null;
                                    }
                                })
                                .on("keydown", function(e) {
                                    if (e.keyCode in KEYS.arrows && $scope.activeCell) {
                                        var direction = KEYS.arrows[e.keyCode];
                                        changeActiveCell(direction);
                                        e.preventDefault();
                                    }
                                });
                        }

                        init();
                    }
                };
            }
        ])

    ;
})(window.angular);
